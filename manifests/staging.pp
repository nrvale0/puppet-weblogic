class weblogic::staging inherits ::weblogic::params {

  $source = $::weblogic::source
  validate_re($source, '^.+$')
  
  require ::staging

  staging::file { 'weblogic-installer.jar':
    source  => $source,
    timeout => '3000',
  }

}
