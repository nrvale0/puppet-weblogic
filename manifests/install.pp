class weblogic::install inherits ::weblogic::params {

  $wls_user = $::weblogic::wls_user
  $wls_group = $::weblogic::wls_group
  $staging_dir = $::weblogic::staging_dir
  $install_dir = $::weblogic::install_dir
  $inventory_dir = $::weblogic::inventory_dir

  multi_validate_re($wls_user, $wls_group, '^.+$')
  validate_absolute_path($staging_dir, $install_dir, $inventory_dir)

  require ::jdk_oracle

  Exec { logoutput => true, path => ['/bin','/sbin','/usr/bin','/usr/sbin','/usr/local/bin'], }

  user { $wls_user: ensure => present, gid => $wls_group, }
  group { $wls_group: ensure => present, }

  exec { 'create Oracle install dir':
    command => "mkdir -p ${install_dir}",
    creates => $install_dir,
  } ->

  file { 'Oracle install dir':
    path => $install_dir,
    ensure => directory,
    owner => 'root',
    group => $wls_group,
    mode => '0775',
  }
  
  exec { 'create Oracle inventory dir':
    command => "mkdir -p ${inventory_dir}",
    creates => $inventory_dir,
  } ->
  
  file { 'Oracle inventory dir':
    path => $inventory_dir,
    ensure => 'directory',
    owner  => $wls_user,
    group  => $wls_group,
  }

  file { '/etc/oracle':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { 'Oracle inventory file':
    ensure  => file,
    path    => '/etc/oracle/oraInst.loc',
    owner   => $wls_user,
    group   => $wls_group,
    mode    => '0755',
    content => template("${module_name}/oraInst.loc.erb"),
  }

  file { 'WebLogic installer responses file':
    ensure  => file,
    path    => "${staging_dir}/wls_installer_responses.txt",
    owner   => $wls_user,
    group   => $wls_group,
    mode    => '0700',
    content => template("${module_name}/wls_installer_responses.txt.erb"),
  }

  $installcmd = "java -d64 -jar ./weblogic-installer.jar -silent -responsefile ${staging_dir}/wls_installer_responses.txt -invPtrLoc /etc/oracle/oraInst.loc"
  exec { 'inst0OBall WebLogic jar':
    cwd     => $staging_dir,
    command => $installcmd,
    user    => $wls_user,
    group   => $wls_group,
    creates => "${install_dir}/weblogic",
    require => File['Oracle inventory file', 'WebLogic installer responses file'],
#    noop => true,
  }
}
